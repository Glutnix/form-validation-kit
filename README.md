# Form Validation Kit

This script can be used to validate forms. It is easy to extend the `validateField` function to validate all kinds of fields.

## Usage
```html
<form … id="contact-form">
…
</form>

…

<script src="form-validation-kit.js"></script>
<script>
document.addEventListener("DOMContentLoaded", function() {
	addFormValidation(document.querySelector("#contact-form"));
});
</script>
```

See `contact-form.html` for a working example.

## Supported Fields
* `<input type="text">` and `search`,`url`,`tel`,`password` -- supports `minlength`, `maxlength` attributes
* `<input type="email">` -- if not blank, must be an email address
* `<input type="checkbox">` -- if required, must be checked
* `<input type="number">` -- supports `min`, `max` attributes for numeric values
* `<textarea>` -- supports `minlength`, `maxlength` attributes
* `<select>` -- invalid options have a blank value, i.e. `<option value="">- pick one- </option>`

## Validation types:
* `required` -- add the required attribute to a field to ensure it is not empty, or for checkboxes, is checked, or for select boxes, has option with a non-blank value.
* `minlength="2"` and `maxlength="10"` for minimum and maximum character length
* `min="0"` and `max="100"` on `number` fields for numeric minimum and maximum values.
* `data-fv-match="password1"` to ensure this field has an identical value as the other field

## Error Messages
Every individual non-button field must have a page-unique `id=""` attribute, along with a matching element with the same id followed by `-error`. 

These error elements are **required** for non-button fields even if they are not being validated.

Example `username` and `username-error`:

```html
<input id="username" name="username" type="text" required minlength="2">
<div id="username-error"></div>
```

### Debugging

If your form is submitting without properly validating, you probably have errors. Open your browser's JavaScript console and enable the 'persist' or 'preserve log' option, then try submitting your form. You should see any errors you might have. 

With this preserve option enabled, be sure to clear your console of errors when necessary so you don't get confused by 'stale' errors.
